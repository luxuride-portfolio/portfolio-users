import type { PageLoad } from './$types';
import { getUser } from '$lib/api/users/get';
import { getUserPosts } from '$lib/api/posts/get';
import { error } from '@sveltejs/kit';

export const load: PageLoad = async ({ parent, params: { user } }) => {
	const { supabase, queryClient } = await parent();
	const userData = await getUser(supabase, user);
	if (userData.error) throw error(404);
	await queryClient.prefetchQuery({
		queryKey: ['user', user],
		queryFn: async () => userData
	});
	await queryClient.prefetchQuery({
		queryKey: ['posts', user],
		queryFn: async () => await getUserPosts(supabase, user)
	});
};
