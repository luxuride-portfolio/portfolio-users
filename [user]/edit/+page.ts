import { error } from '@sveltejs/kit';
import { getUser } from '$lib/api/users/get';
import type { PageLoad } from './$types';

export const load: PageLoad = async ({ parent, params: { user } }) => {
	const { session, supabase, queryClient } = await parent();
	if (!session) throw error(403);
	if (!(await supabase.rpc('is_admin')).data && !(await supabase.rpc('is_poster')).data)
		throw error(401);
	const userData = await getUser(supabase, user);
	if (userData.error) throw error(404);
	await queryClient.prefetchQuery({
		queryKey: ['user', user],
		queryFn: async () => userData
	});
};
